package com.example.coba.Model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "T_KELUARGA")
public class CobaModel {
	
	@Id
	@Column(name = "ID")
	private int id;
	@Column(name = "NAMAKELUARGA")
	private String namaKeluarga;
	@Column(name = "JUMLAHKELUARGA")
	private int jumlahKeluarga;
	@Column(name = "KELURAHAN")
	private String kelurahan;
	@Column(name = "TANGGAL")
	private Date tanggal;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNamaKeluarga() {
		return namaKeluarga;
	}
	public void setNamaKeluarga(String namaKeluarga) {
		this.namaKeluarga = namaKeluarga;
	}
	public int getJumlahKeluarga() {
		return jumlahKeluarga;
	}
	public void setJumlahKeluarga(int jumlahKeluarga) {
		this.jumlahKeluarga = jumlahKeluarga;
	}
	public String getKelurahan() {
		return kelurahan;
	}
	public void setKelurahan(String kelurahan) {
		this.kelurahan = kelurahan;
	}
	public Date getTanggal() {
		return tanggal;
	}
	public void setTanggal(Date tanggal) {
		this.tanggal = tanggal;
	}
	
	


}
