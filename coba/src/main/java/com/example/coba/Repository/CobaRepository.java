package com.example.coba.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


import com.example.coba.Model.CobaModel;

public interface CobaRepository extends JpaRepository<CobaModel, String> {

	@Query("select f from CobaModel f")
	List<CobaModel> repoRead();
	
	@Query("Select P from CobaModel P where P.namaKeluarga like %?1%")
	List<CobaModel> readName(String searchName);

	void deleteById(int id);
}
