package com.example.coba.Service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.coba.Model.CobaModel;
import com.example.coba.Repository.CobaRepository;

@Transactional
@Service
public class CobaService {

	@Autowired
	private CobaRepository cobaRepository;
	
	public void create(CobaModel cobaModel) {
		cobaRepository.save(cobaModel);
	}
	
	public List<CobaModel>read(){
		return this.cobaRepository.repoRead();
	}
	
	public List<CobaModel> readName(String searchName) {
		return this.cobaRepository.readName(searchName);
		
	}

	public void update(CobaModel cobaModel) {
		cobaRepository.save(cobaModel);
	}

	public void delete(int id) {
		cobaRepository.deleteById(id);
	}
}
