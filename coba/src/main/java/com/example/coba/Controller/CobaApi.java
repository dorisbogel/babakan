package com.example.coba.Controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.coba.Model.CobaModel;
import com.example.coba.Service.CobaService;

@RestController
@RequestMapping("/api/cobaaja")
public class CobaApi {

	@Autowired
	private CobaService cobaService;
	
	
	@PostMapping("/post")
	@ResponseStatus(code = HttpStatus.CREATED)
	public Map<String, Object> postAPI(@RequestBody CobaModel cobaModel){
		this.cobaService.create(cobaModel);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("succes", Boolean.TRUE);
		map.put("pesan", "coba sudah diPost");
		return map;
	}
	
	@GetMapping("/get")
	@ResponseStatus(code = HttpStatus.OK)
	public List<CobaModel> getAPI(){
		List<CobaModel> cmList = new ArrayList<CobaModel>();
		cmList = this.cobaService.read();
		return cmList;
	}
	
	@PutMapping("/put")
	@ResponseStatus(code = HttpStatus.OK)
	public Map<String, Object> putAPI(@RequestBody CobaModel cobaModel){
		this.cobaService.update(cobaModel);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("pesan", Boolean.TRUE);
		map.put("pesan", "telah terUpdate");
		return map;
	}
	
	@DeleteMapping("/delete/{id}")
	@ResponseStatus(code = HttpStatus.GONE)
	public Map<String, Object> deleteAPI(@PathVariable int id){
		this.cobaService.delete(id);
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("succes", Boolean.TRUE);
		map.put("pesan", "data sudah di hapus"+id+"deleted");
		return map;
	}
}
