package com.example.coba.Controller;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.coba.Model.CobaModel;
import com.example.coba.Service.CobaService;

@Controller
public class HomeController {

	@Autowired
	private CobaService cobaService;
	
	@RequestMapping("/")
	public String home() {
		String html ="home";
		return html;
	}
	
	@RequestMapping("/index")
	public String index() {
		String html ="login/index";
		return html;
	}
	
	@RequestMapping("/hadir")
	public String hadir() {
		String html ="absen/hadir";
		return html;
	}
	
	@RequestMapping("/profil")
	public String profil() {
		String html ="profil/profil";
		return html;
	}
	
	@RequestMapping("/gallery")
	public String gallery() {
		String html ="gallery/gallery";
		return html;
	}
	
	@RequestMapping("/keluarga")
	public String data(Model model) {
		
		List<CobaModel> cobaModelList = new ArrayList<CobaModel>();
		cobaModelList = this.cobaService.read();
		model.addAttribute("cobaModelList", cobaModelList);
		
		String html ="keluarga/data";
		return html;
	}
	
}
