package com.example.coba.Controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.coba.Model.CobaModel;
import com.example.coba.Repository.CobaRepository;
import com.example.coba.Service.CobaService;

@Controller
@RequestMapping("/keluarga")
public class CobaController {
	
	@Autowired
	private CobaService cobaService;

	@Autowired
	CobaRepository cobaRepository;
	
	@RequestMapping("/addhtml")
	public String addHtml() {
		

		String html = "keluarga/add";
		return html;

	}
	
	@RequestMapping("/add")
	public String add(HttpServletRequest request, Model model) {
		int id = Integer.parseInt(request.getParameter("id"));
		String namaKeluarga = request.getParameter("namaKeluarga");
		int jumlahKeluarga = Integer.parseInt(request.getParameter("jumlahKeluarga"));
		String kelurahan = request.getParameter("kelurahan");
		Date tanggal = new Date();
		
		CobaModel cobaModel = new CobaModel();
		cobaModel.setId(id);
		cobaModel.setNamaKeluarga(namaKeluarga);
		cobaModel.setJumlahKeluarga(jumlahKeluarga);
		cobaModel.setKelurahan(kelurahan);
		cobaModel.setTanggal(tanggal);
		
		this.cobaService.create(cobaModel);
		
		this.read(model);
		
		String html = "/keluarga/data";
		return html;
	}

	public void read(Model model) {
		List<CobaModel> cobaModelList = new ArrayList<CobaModel>();
		cobaModelList = this.cobaService.read();
		model.addAttribute("cobaModelList", cobaModelList);
	}
	
	@RequestMapping("search/name")
	public String searchName(HttpServletRequest request, Model model) {
		String searchName = request.getParameter("searchName");
		
		List<CobaModel> cobaModelList = new ArrayList<CobaModel>();
		cobaModelList = this.cobaService.readName(searchName);
		model.addAttribute("cobaModelList", cobaModelList);
		
		String html = "keluarga/data";
		return html;
	}

}
